package com.example.contactsandorders.SQLite;

import android.app.Application;

import com.example.contactsandorders.CustomClasses.Contact;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SharedViewModel extends AndroidViewModel {
    MutableLiveData<Contact> contact = new MutableLiveData<>();

    private ContactRepository repository;
    private LiveData<List<Contact>> allContacts;

    public SharedViewModel(@NonNull Application application) {
        super(application);

        repository = new ContactRepository(application);
        allContacts = repository.getAllContacts();
    }

    public void insert(Contact contact){
        repository.insert(contact);
    }

    public void update(Contact contact){
        repository.update(contact);
    }

    public void delete(Contact contact){
        repository.delete(contact);
    }

    public void deleteAllContacts(){
        repository.deleteAllContacts();
    }

    public LiveData<List<Contact>> getAllContacts(){
        return allContacts;
    }

    public void setContact(Contact contact){
        this.contact.setValue(contact);
    }

    public LiveData<Contact> getContact(){
        return this.contact;
    }

}

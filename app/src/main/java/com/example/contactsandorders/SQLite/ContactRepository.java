package com.example.contactsandorders.SQLite;

import android.app.Application;
import android.os.AsyncTask;

import com.example.contactsandorders.CustomClasses.Contact;

import java.util.List;

import androidx.lifecycle.LiveData;

public class ContactRepository {
    private ContactDao contactDao;
    private LiveData<List<Contact>> allContacts;

    public ContactRepository(Application app){
        ContactDatabase database = ContactDatabase.getInstance(app);

        contactDao = database.contactDao();
        allContacts = contactDao.getAllContacts();
    }

    public void insert(Contact contact){
        new InsertContactAsyncTask(contactDao).execute(contact);
    }

    public void update(Contact contact){
        new UpdateContactAsyncTask(contactDao).execute(contact);
    }

    public void delete(Contact contact){
        new DeleteContactAsyncTask(contactDao).execute(contact);
    }

    public void deleteAllContacts(){
        new DeleteAllContactsAsyncTask(contactDao).execute();
    }

    public LiveData<List<Contact>> getAllContacts(){
        return allContacts;
    }

    private static class InsertContactAsyncTask extends AsyncTask<Contact, Void, Void>{
        private ContactDao contactDao;

        private InsertContactAsyncTask (ContactDao contactDao){
            this.contactDao = contactDao;
        }
        @Override
        protected Void doInBackground(Contact... contacts) {
            contactDao.insert(contacts[0]);
            return null;
        }
    }

    private static class UpdateContactAsyncTask extends AsyncTask<Contact, Void, Void>{
        private ContactDao contactDao;

        private UpdateContactAsyncTask (ContactDao contactDao){
            this.contactDao = contactDao;
        }
        @Override
        protected Void doInBackground(Contact... contacts) {
            contactDao.update(contacts[0]);
            return null;
        }
    }

    private static class DeleteContactAsyncTask extends AsyncTask<Contact, Void, Void>{
        private ContactDao contactDao;

        private DeleteContactAsyncTask (ContactDao contactDao){
            this.contactDao = contactDao;
        }
        @Override
        protected Void doInBackground(Contact... contacts) {
            contactDao.delete(contacts[0]);
            return null;
        }
    }

    private static class DeleteAllContactsAsyncTask extends AsyncTask<Contact, Void, Void>{
        private ContactDao contactDao;

        private DeleteAllContactsAsyncTask (ContactDao contactDao){
            this.contactDao = contactDao;
        }
        @Override
        protected Void doInBackground(Contact... contacts) {
            contactDao.deleteAllContacts();
            return null;
        }
    }

}

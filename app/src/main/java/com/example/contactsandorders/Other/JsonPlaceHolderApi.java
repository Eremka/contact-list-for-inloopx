package com.example.contactsandorders.Other;

import com.example.contactsandorders.CustomClasses.ContactsList;
import com.example.contactsandorders.CustomClasses.OrderList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface JsonPlaceHolderApi {

    @GET("contact")
    Call<ContactsList> getContactsList();

    @GET("{id}")
    Call<OrderList> getOrderList(@Path("id") int contactId);
}

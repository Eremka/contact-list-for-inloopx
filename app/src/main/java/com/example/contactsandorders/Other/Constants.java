package com.example.contactsandorders.Other;

public class Constants {

    public static final String BASE_CONTACT_URL = "https://inloop-contacts.appspot.com/_ah/api/contactendpoint/v1/";
    public static final String BASE_ORDER_URL = "https://inloop-contacts.appspot.com/_ah/api/orderendpoint/v1/order/";
    public static final String BASE_PICTURE_URL = "http://inloop-contacts.appspot.com/";

    public static final String ADD_CONTACT_ACTIVITY_NAME = "Add a Contact";
    public static final String CONTACT_LIST_ACTIVITY_NAME = "Contacts";

    public static final String CACHE_CONTROL = "Cache-Control";

    public static final String PARSE_CONTACT = "parse_contact";


}

package com.example.contactsandorders.ActivitiesAndFragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.contactsandorders.CustomClasses.CheckConnectivity;
import com.example.contactsandorders.CustomClasses.Contact;
import com.example.contactsandorders.CustomClasses.Order;
import com.example.contactsandorders.CustomClasses.OrderList;
import com.example.contactsandorders.Other.Constants;
import com.example.contactsandorders.Other.JsonPlaceHolderApi;
import com.example.contactsandorders.R;
import com.example.contactsandorders.SQLite.SharedViewModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnOrderListFragmentListener}
 * interface.
 */
public class OrderListFragment extends Fragment {

    private ArrayList<Order> ordersList;
    private OnOrderListFragmentListener mListener;

    private TextView phoneTextView, emptyTextView;
    private RecyclerView recyclerView;
    Contact currentContact;


    private int days = 15;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OrderListFragment() {
    }

    @SuppressWarnings("unused")
    public static OrderListFragment newInstance(int columnCount) {
        OrderListFragment fragment = new OrderListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_list, container, false);

        phoneTextView = view.findViewById(R.id.order_list_phone_tv);
        emptyTextView = view.findViewById(R.id.empty_view_order_list);
        ordersList = new ArrayList();

        // Setting the back button once more in the toolbar. The toolbar is always there.
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set the adapter
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.order_list_rv);
        recyclerView.setAdapter(new MyOrderRecyclerViewAdapter(ordersList, mListener));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        SharedViewModel sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        currentContact = sharedViewModel.getContact().getValue();

        // Setting the title in the toolbar and the phone number in the TextView.
        getActivity().setTitle(currentContact.getName());
        phoneTextView.setText(currentContact.getPhoneNumber());

        if (!CheckConnectivity.hasInternetConnection(getContext())) {
            loadOfflineData();
        } else {
            loadOnlineData();
        }

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOrderListFragmentListener) {
            mListener = (OnOrderListFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ContactListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void loadOnlineData(){
        long cacheSize = (5 * 1024 * 1024);
        File directory = getContext().getCacheDir();

        OkHttpClient client = new OkHttpClient.Builder()
                .cache(new Cache(directory, cacheSize))
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        if (CheckConnectivity.hasInternetConnection(getContext())){
                            request.newBuilder().header(Constants.CACHE_CONTROL,
                                    "public, max-age=" + 5).build();
                        } else {
                            request.newBuilder().header(Constants.CACHE_CONTROL,
                                    "public, only-if-cached, max-stale=" + 60 * 60 * 24 * days)
                                    .build();
                        }
                        return chain.proceed(request);
                    }
                }).build();

        /*
        This has not been tested. The author would not know whether this request is working.
         */
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_ORDER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<OrderList> call = jsonPlaceHolderApi.getOrderList(currentContact.getId());
        call.enqueue(new Callback<OrderList>() {
            @Override
            public void onResponse(Call<OrderList> call, Response<OrderList> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: " + response.code());

                    OrderList orderList = response.body();
                    if (orderList.getOrderList() != null && orderList.getOrderList().size() != 0) {
                        ordersList.addAll(orderList.getOrderList());
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                    for (Order o : orderList.getOrderList()) {
                        Log.d(TAG, "onResponse: " + o.toString());
                    }

                } else {
                    loadOfflineData();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.something_has_gone_wrong)
                                    + response.code(),
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OrderList> call, Throwable t) {
                loadOfflineData();

                Toast.makeText(getActivity(),
                        getResources().getString(R.string.something_has_gone_totally_wrong),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadOfflineData(){
        if (ordersList.size() == 0){
            recyclerView.setVisibility(View.GONE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnOrderListFragmentListener {
        void onOrderListFragmentInteraction(Order item);
    }
}

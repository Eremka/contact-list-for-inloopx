package com.example.contactsandorders.ActivitiesAndFragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.contactsandorders.CustomClasses.CheckConnectivity;
import com.example.contactsandorders.CustomClasses.Contact;
import com.example.contactsandorders.CustomClasses.ContactsList;
import com.example.contactsandorders.Other.Constants;
import com.example.contactsandorders.Other.JsonPlaceHolderApi;
import com.example.contactsandorders.R;
import com.example.contactsandorders.SQLite.SharedViewModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ContactListFragment extends Fragment {

    private SharedViewModel sharedViewModel;

    private static final String TAG = ContactListFragment.class.getSimpleName();
    private ArrayList<Contact> contactList = new ArrayList<>();

    private int days = 15;

    private RecyclerView recyclerView;
    private TextView emptyView;
    MyContactRecyclerViewAdapter adapter;

    private ContactListFragmentListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ContactListFragment() {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout_menu:
                // Will terminate the app
                getActivity().finish();
                return true;
            case R.id.add_contact_menu:
                // This is the navigation to the Create Contact Fragment.
                // This activity is back stacked so that when the user hits the back button,
                // the user will get back to this activity.
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();

                CreateContactFragment createContactFragment = new CreateContactFragment();
                transaction.setCustomAnimations(android.R.anim.slide_in_left,
                        android.R.anim.slide_out_right, R.anim.slide_in_right,
                        R.anim.slide_out_left);
                transaction.replace(R.id.main_activity_ll, createContactFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: has been called. Activity started");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

        // Removes the back button from the navigation bar.
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        // Sets the options menu on the toolbar
        // Sets the title on the toolbar.
        getActivity().setTitle(Constants.CONTACT_LIST_ACTIVITY_NAME);
        setHasOptionsMenu(true);

        // Gets the views and stores them in a variable.
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.contact_list_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        adapter = new MyContactRecyclerViewAdapter(contactList, mListener);
        recyclerView.setAdapter(adapter);

        // Sets an empty view when there are no entries.
        if (contactList.size() == 0) {
            emptyView = view.findViewById(R.id.empty_view);

            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        // Checks whether the app has internet. When the app has no internet, the data form the
        // cache will be used. Else the data from the online database will be pulled.
        if (!CheckConnectivity.hasInternetConnection(getContext())) {
            loadOfflineData();
        } else {
            loadOnlineData();
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: has been called");
        super.onAttach(context);
        if (context instanceof ContactListFragmentListener) {
            mListener = (ContactListFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ContactListFragmentListener");
        }
    }

    private void loadOnlineData() {
        /*
          This method is only here to show you how I would cache the data. Obviously there is
          never going to be any data cached as all API calls will be unsuccessful.
         */

        long cacheSize = (5 * 1024 * 1024);
        File directory = getContext().getCacheDir();

        OkHttpClient client = new OkHttpClient.Builder()
                .cache(new Cache(directory, cacheSize))
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        if (CheckConnectivity.hasInternetConnection(getContext())){
                            request.newBuilder().header(Constants.CACHE_CONTROL,
                                    "public, max-age=" + 5).build();
                        } else {
                            request.newBuilder().header(Constants.CACHE_CONTROL,
                                    "public, only-if-cached, max-stale=" + 60 * 60 * 24 * days)
                                    .build();

                        }
                        return chain.proceed(request);
                    }
                }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_CONTACT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<ContactsList> call = jsonPlaceHolderApi.getContactsList();
        call.enqueue(new Callback<ContactsList>() {
            @Override
            public void onResponse(Call<ContactsList> call, Response<ContactsList> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: " + response.code());

                    ContactsList contactsList = response.body();
                    if (contactsList.getContactList() != null && contactsList.getContactList().size() != 0) {
                        contactList.addAll(contactsList.getContactList());
                    } else {
                        emptyView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                    for (Contact c : contactsList.getContactList()) {
                        Log.d(TAG, "onResponse: " + c.toString());
                    }

                } else {
                    loadOfflineData();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.something_has_gone_wrong)
                                    + response.code(),
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactsList> call, Throwable t) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.something_has_gone_totally_wrong),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadOfflineData(){
        /*
        This method is here so that at least some data is populated in the RecyclerView.
         */

        // Loads the SharedViewModel class.
        sharedViewModel.getAllContacts().observe(getActivity(), new Observer<List<Contact>>() {
            @Override
            public void onChanged(List<Contact> contacts) {
                contactList.clear();
                contactList.addAll(contacts);
                if (contacts.size() != 0) {
                    for (Contact c: contacts) {
                        Log.d(TAG, "onChanged: Contact: " + c.toString());
                    }

                    emptyView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface ContactListFragmentListener {
        void contactListFragmentInteraction(Contact contact);
    }
}

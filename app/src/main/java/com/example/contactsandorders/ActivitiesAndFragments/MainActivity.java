package com.example.contactsandorders.ActivitiesAndFragments;

import android.os.Bundle;

import com.example.contactsandorders.CustomClasses.Contact;
import com.example.contactsandorders.CustomClasses.Order;
import com.example.contactsandorders.R;
import com.example.contactsandorders.SQLite.SharedViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

/*
The author of this application knows that the app is not finished. The main methods have been created
but some finishing touches are necessary. However, as I was informed that the api would not get online
and that my application was unsuccessful because of the vacancies no longer being open, I will not
continue to work on this application. I will not finish it, mainly because I cannot test the app. This
is particularly true for the POST of the contact. This is the only TODO still in the app.

I am however, requesting feedback on this work as I would be very helpful for any applications in
the future. It also might help me in a possible application for a mobile tester position in Avast/InloopX
It would be greatly appreciated if you could provide me the feedback. Please do not hold back. It
is incredible difficult ot insult a Dutchy. Besides, the more detailed (and harsher) the feedback,
the more I will learn from it.

Thank you in advance.
 */

public class MainActivity extends AppCompatActivity implements
        ContactListFragment.ContactListFragmentListener,
        OrderListFragment.OnOrderListFragmentListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private SharedViewModel sharedViewModel;

    // Navigation so that the back button on the bottom does the same as the back button in the
    // toolbar.
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedViewModel = ViewModelProviders.of(this).get(SharedViewModel.class);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        ContactListFragment contactListFragment = new ContactListFragment();
        transaction.replace(R.id.main_activity_ll, contactListFragment);
        transaction.commit();
    }

    @Override
    public void contactListFragmentInteraction(Contact contact) {
        sharedViewModel.setContact(contact);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        OrderListFragment orderListFragment = new OrderListFragment();
        transaction.setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right, R.anim.slide_in_right,
                R.anim.slide_out_left);
        transaction.replace(R.id.main_activity_ll, orderListFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onOrderListFragmentInteraction(Order item) {

    }
}

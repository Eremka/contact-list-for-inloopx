package com.example.contactsandorders.ActivitiesAndFragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.contactsandorders.CustomClasses.Contact;
import com.example.contactsandorders.Other.Constants;
import com.example.contactsandorders.R;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class MyContactRecyclerViewAdapter extends RecyclerView.Adapter<MyContactRecyclerViewAdapter.ViewHolder> {

    private final List<Contact> values;
    private final ContactListFragment.ContactListFragmentListener listener;

    public MyContactRecyclerViewAdapter(List<Contact> items, ContactListFragment.ContactListFragmentListener listener) {
        this.values = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = values.get(position);
        holder.nameView.setText(values.get(position).getName());
        holder.phoneView.setText(values.get(position).getPhoneNumber());

        if (values.get(position).getPicUrl() == null) {
            // This happens when there is no image set on the Contact Record
            Glide.with(holder.view.getContext())
                    .load("")
                    .placeholder(holder.view.getContext().getResources().getDrawable(R.mipmap.alien))
                    .centerCrop()
                    .into(holder.imageView);
        } else {
            // When there is a url on the contact record, the image is loaded from the internet
            // and loaded into the image view.
            // The picture could be resized etc, but there is no way of testing at the moment
            // what is necessary for the perfect image view content as the api is down.
            Glide.with(holder.view.getContext())
                    .load(Constants.BASE_PICTURE_URL + values.get(position).getPicUrl())
                    .centerCrop()
                    .into(holder.imageView);
        }


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    listener.contactListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final ImageView imageView;
        public final TextView nameView;
        public final TextView phoneView;
        public Contact mItem;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            imageView = view.findViewById(R.id.contact_image_view);
            nameView = view.findViewById(R.id.item_number);
            phoneView = view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + phoneView.getText() + "'";
        }
    }
}

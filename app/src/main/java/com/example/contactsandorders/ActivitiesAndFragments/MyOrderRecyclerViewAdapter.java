package com.example.contactsandorders.ActivitiesAndFragments;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.contactsandorders.ActivitiesAndFragments.OrderListFragment.OnOrderListFragmentListener;
import com.example.contactsandorders.CustomClasses.Order;
import com.example.contactsandorders.R;

import java.util.List;

public class MyOrderRecyclerViewAdapter extends RecyclerView.Adapter<MyOrderRecyclerViewAdapter.ViewHolder> {

    private final List<Order> mValues;
    private final OnOrderListFragmentListener mListener;

    public MyOrderRecyclerViewAdapter(List<Order> items, OnOrderListFragmentListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.orderNameTv.setText(mValues.get(position).getOrderName());
        holder.contentView.setText(mValues.get(position).getOrderCount());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onOrderListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView orderNameTv;
        public final TextView contentView;
        public Order mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            orderNameTv = view.findViewById(R.id.order_item_name_tv);
            contentView = view.findViewById(R.id.order_quantity_tv);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + contentView.getText() + "'";
        }
    }
}

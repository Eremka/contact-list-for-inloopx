package com.example.contactsandorders.ActivitiesAndFragments;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.contactsandorders.CustomClasses.CheckConnectivity;
import com.example.contactsandorders.CustomClasses.Contact;
import com.example.contactsandorders.Other.Constants;
import com.example.contactsandorders.R;
import com.example.contactsandorders.SQLite.SharedViewModel;

public class CreateContactFragment extends Fragment {

    private SharedViewModel sharedViewModel;

    private static final String TAG = CreateContactFragment.class.getSimpleName();
    private EditText nameEt, phoneEt;

    public CreateContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Setting the back button on the toolbar. Toolbar is always true
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        inflater.inflate(R.menu.save_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_menu:
                if (CheckConnectivity.hasInternetConnection(getActivity())) {
                    // Saving the contact. First the contact is created.
                    Contact contact = new Contact();
                    contact.setName(nameEt.getText().toString().trim());
                    contact.setPhoneNumber(phoneEt.getText().toString().trim());

                    // TODO this bit of code needs te be replaced with a save online function.
                    // Here the contact is transferred in to ContentValues and saved through the
                    // contentResolver.
                    sharedViewModel.insert(contact);
                    getActivity().onBackPressed();
                } else {
                    // Let the user know there is no internet connectivity.
                    // New contact will not be saved when there is no internet.
                    Toast.makeText(getActivity(),
                            getActivity().getResources().getString(R.string.internet_for_contact),
                            Toast.LENGTH_SHORT).show();
                }
                Log.d(TAG, "onOptionsItemSelected: action saved pressed");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Setting the title and the options menu in the toolbar
        getActivity().setTitle(Constants.ADD_CONTACT_ACTIVITY_NAME);
        setHasOptionsMenu(true);

        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_contact, container, false);

        // Setting the variables for the fields from the UI.
        nameEt = view.findViewById(R.id.create_contact_name_et);
        phoneEt = view.findViewById(R.id.create_contact_phone_et);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

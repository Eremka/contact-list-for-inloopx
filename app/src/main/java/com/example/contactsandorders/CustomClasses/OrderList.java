package com.example.contactsandorders.CustomClasses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderList {
    @SerializedName("items")
    private List<Order> orderList;

    public List<Order> getOrderList() {
        return orderList;
    }
}

package com.example.contactsandorders.CustomClasses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactsList {

    @SerializedName("items")
    private List<Contact> contactList;

    public List<Contact> getContactList() {
        return contactList;
    }
}

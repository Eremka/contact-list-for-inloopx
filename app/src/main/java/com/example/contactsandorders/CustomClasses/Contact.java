package com.example.contactsandorders.CustomClasses;

import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "contact_table")
public class Contact {
    @SerializedName("id")
    @PrimaryKey
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("phone")
    @ColumnInfo(name = "phone_number")
    private String phoneNumber;

    @SerializedName("pictureUrl")
    @ColumnInfo(name = "picture_url")
    private String picUrl;

    @Ignore
    public Contact(){
        // Empty constructor
    }

    // Constructor for the Dao
    public Contact(String name, String phoneNumber, String picUrl) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.picUrl = picUrl;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id= " + id +
                ", name= '" + name + '\'' +
                ", phoneNumber= '" + phoneNumber + '\'' +
                ", picUrl= '" + picUrl + '\'' +
                '}';
    }
}

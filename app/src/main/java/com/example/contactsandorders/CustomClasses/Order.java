package com.example.contactsandorders.CustomClasses;

import com.google.gson.annotations.SerializedName;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "order_table")
public class Order {

    @PrimaryKey
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("count")
    private int count;

    @Ignore
    public Order() {

    }

    // Constructor for the DAO
    public Order(int id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderName() {
        return name;
    }

    public void setOrderName(String name) {
        this.name = name;
    }

    public int getOrderCount() {
        return count;
    }

    public void setOrderCount(int count) {
        this.count = count;
    }
}

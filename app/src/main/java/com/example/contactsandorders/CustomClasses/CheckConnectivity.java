package com.example.contactsandorders.CustomClasses;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class CheckConnectivity {

    private static final String TAG = CheckConnectivity.class.getSimpleName();

    public static boolean hasInternetConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            NetworkInfo info = cm.getActiveNetworkInfo();

            Log.d(TAG, "Internet is on: " + info.isConnected());
            return info.isConnected();
        } catch (NullPointerException e){
            Log.d(TAG, "Cannot find the internet connection");
            return false;
        }
    }
}
